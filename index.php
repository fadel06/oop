<?php

require_once 'animal.php';
require_once 'Frog.php';
require_once 'Ape.php';

$sheep = new Animal("shaun");

echo "<h3>Release 0</h3>";
echo $sheep->name. "<br>"; // "shaun"
echo $sheep->legs. "<br>"; // 2
var_dump($sheep->cold_blooded); // false
echo "<br> <br>";

echo "<h3>Release 1 </h3>";
$sungokong = new Ape("kera sakti");
echo $sungokong->name. "<br>";
echo $sungokong->yell();
echo "<br><br>";
$kodok = new Frog("buduk");
echo $kodok->name. "<br>";
echo $kodok->jump();


?>
