<?php

class Animal
{
  public $name;
  public $legs = 2;
  public $cold_blooded = false;

  function __construct($name)
  {
    $this->name = $name;
  }

  public function set_name($name){
    $this->name = $name;
  }

  public function get_name($name){
    return $this->name;
  }

}

?>
